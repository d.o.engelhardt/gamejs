# GameJS v1.0
###### (c) 2021 - Oliver Engelhardt

Dieses kleine Framework soll hauptsächlich das Entwickeln von Spielen auf JavaScript-Basis
erleichtern. Hierzu wurden einige Probleme die einem Entwickler dabei begegenen
bereits gelöst und die Lösungen stehen dem Entwickler min Rahmen eines schlanken
Frameworks zur Verfügung. So kann sämtliche Energie in das eigene Projekt gesteckt werden,
statt in das ständige Herumärgern mit ellenlangen Dokumentationen, Ausnahmefällen und
Browserkomparibilitätsproblemen. So soll dieses Framework lediglich als Grundstein
für Projekte dienen. Seine Architektur ist recht flach gehalten und es ist beliebig im
Funktionsumfang erweiterbar. Selbst dafür liefert es gleich selbst die Funktionen mit.

<br>  

### Nutzung

---
```HTML  
<!DOCTYPE html>  
<html>  
   <head>  
      <meta charset="utf-8">  
      <script src="js/game.js"></script>  
   </head>  
   <body>      
      <game script="beliebiger_pfad/mein_tolles_spiel.js"></game>         
   </body>  
</html>  
```  
Das Framework stellt ein neues HTML-Tag namens `<game>` zur Verfügung.
Es handelt sich hierbei um einen Kontainer, ähnlich dem nativen `<div>`.
Dieser wird beim Laden des Dokuments mit einem `<canvas>` und einem
`<script>` befüllt. Im Script-Tag wird jenes Skript geladen, welches
im Attribut `script="..."` angegeben wurde. Dieses Skript benötigt keinen
besonderen Aufbau. Es wird schlicht, direkt nach dem Laden des Game-Elements
ausgeführt. Jedes Game-Element verfügt über seinen eigenen Scope, sodass
die parallele Nutzung mehrerer Game-Elemente gleichzeitig möglich ist,
ohne dass sich gleichnamige Variablen ins Gehege kommen. Die einzige
Ausnahme bilden hierbei selbstverständlich die Globalen `window` und
`document`. Innerhalb dieses Skriptes steht eine `include`-Funktion
zur Verfügung, die genutzt werden kann, um weitere Skripte hinzu zu laden.
Dazu mehr unter dem Stichpunkt [__globale Funktionen__](#globale-funktionen).
Zusätzlich wird dem Game-Skript das Objekt `game` übergeben.
Es dient zur Interaktion mit dem Game-Element und zum Auslesen von
Nutzereingaben (Maus, Tastaur, etc.). Dessen Aufbau und Funktionsweise
soll im Folgenden erklärt werden.

<br>  

### Object game

---


* `canvas: HTML-DOM-Element`  
   Canvas Element, das beim Laden des Dokuments im Game-Element erstellt wird. Es handelt sich um ein herkömmliches Canvas-Element und soll als grafischer Output verwendet werden.  
  

* `element: HTML-DOM-Element`  
   das, im HTML enthaltene, Game Element selbst  
  

* `autosize: Boolean`  
   gibt an, ob die Größe des Canvas-Objekts und damit auch die Auflösung automatisch an die Größe des Game-Elements angepasst werden soll, oder nicht.


* `fullscreen: Boolean`  
   zeigt an, ob sich ein Game-Element im Vollbildmodus befindet.  
   __Es ist leider nicht steuerbar, da Browser das automatisierte Umschalten auf Vollbild, aus Sicherheitsgründen unterbinden. Jedoch steht ein Vollbild-Button zur Verfügung der am unteren rechten Rand des Elements eingeblendet werden kann. Über diesen kann der Nutzer selbst, manuell auf Vollbild umschalten.__  
  
  
* `showFullScreenButton: Boolean`  
   gibt an, ob ein Button, über den der Nutzer in den Vollbildmodus wechseln kann, angezeigt werden soll oder nicht.  


* `width: Number`  
   gibt die Breite des Game-Elements an.  


* `height: Number`  
  gibt die Höhe des Game-Elements an.
  
  
* `resolutionWidth: Number`  
  ist eine direkte Weiterleitung von canvas.width und bestimmt die horizontale Auflösung des Canvas-Elements. Ist `autosize` auf true gesetzt, so wird diese automatisch auf die Breite des Game-Elements angepasst.  
  
  
* `resolutionHeight: Number`  
  ist eine direkte Weiterleitung von canvas.height und bestimmt die vertikale Auflösung des Canvas-Elements. Ist `autosize` auf true gesetzt, so wird diese automatisch auf die Höhe des Game-Elements angepasst.  
  
   
* `keepAspectRatio: Boolean`  
   gibt an, ob bei der Skalierung des Game-Elements das Seitenverhältnis des Canvas-Elements beibehalten werden soll. Überschüssiger Platz wird dann klassisch durch schwarze Streifen an den Bildschirmrändern ersetzt.   


* `input: Object`  
  sammelt und stellt sämtliche Nutzereingaben durch die Eingabegräte Tastatur, Maus und Touchscreen bereit:  
  <br>  
  * `key: Object`  
    * `function pressed( Number AsciiKeyCode ): Boolean`  
      gibt einen boolschen Wert zurück, ob die, dem AsciiKeyCode zugeordnete, Taste auf der Tastatur gedrückt ist.  
      <br>
  * `mouse: Object`
      * `x: Number`  
        X-Koordinate des Mauszeigers relativ zum Canvas-Element und dessen Auflösung. Toucheingaben werden auf dieses Attribut gemappt.  
        <br>
      * `y: Number`  
        Y-Koordinate des Mauszeigers relativ zum Canvas-Element und dessen Auflösung. Toucheingaben werden auf dieses Attribut gemappt.  
        <br>  
      * `wheel: Number`  
        Position des Scrollrads. Toucheingaben werden auf dieses Attribut gemappt.  
        <br>   
      * `button: Object`
        * `function pressed( Number KeyNumber ): Boolean`  
          gibt einen boolschen Wert zurück, ob eine Taste der Maus gedrückt ist. Toucheingaben werden auf dieses Attribut gemappt.  
          0 -> linke Maustaste   
          1 -> rechte Maustaste   
          2 -> mittlere Maustaste   
          <br>
* `resources: Object`  
  dient als Cache zum Laden von Bildern oder sonstiger Dateien  
  <br>
  * `loadingStatus: Number`  
    zeigt den prozentualen (0.0 bis 1.0) Fortschritt des Ladevorgangs an.  
    <br>
  * `function loadFile( String name, String url ): void`  
    lädt eine Datei von der angegebenen URL und legt diese im internen Speicher unter dem angegebenen Namen ab. Der Ladevorgang beginnt sofort mit Aufruf dieser Funktion.  
    <br>
  * `function loadImage( String name, String url ): void`  
    lädt ein Bild von der angegebenen URL und legt dieses als Canvas-Objekt im internen Speicher unter dem angegebenen Namen ab. Der Ladevorgang beginnt sofort mit Aufruf dieser Funktion.  
    <br>
  * `function get( String name ): String/Canvas-DOM-Element/null`  
    gibt direkt die im internen Speicher, unter dem angegebenen Namen hinterlegte Resource aus. Ist die Datei noch nicht geladen oder kann nicht gefunden werden, gibt die Funktion null zurück.

<br>  

### globale Funktionen

---
* `function include( String path ): void`  
  dient zum Einbinden weiterer Skripte. Diese Funktion steht nur innerhalb des Game-Objekts zur Verfügung, dort kann sie jedoch in beliebig tiefer Verschachtelung genutzt werden.
  Das Laden der includierten Skripte geschieht mitsamt dem Hauptskript. Diese werden während des Ladevorgangs in das Haupscript direkt mit hineinkopiert, ähnlich wie es bei kompilativen
  Sprachen wie C++ geschieht. Somit sind alle includierten Skripte beim Start des Hauptskriptes voll Verfügbar, sodass eine Prüfung auf Vollständigkeit nicht mehr nötig ist.  
  <br>
  __Hinweise zum Pfad:__  
  - der Pfad kann __relativ__ zum aufrufenden Skript angegeben werden
  - die __absolute__ Angabe einer URL (http://, https://...) ist ebenso möglich
  - beginnt der Pfad mit `/`, so startet der Pfad im Rootverzeichnis (bei der index.html)
  - `../` ermöglicht den Wechsel in den nächst höheren Ordner  
  <br>
   
* `function window.loadFile( String url, function onload=null, function onerror=null, Boolean async=true ): void/String/null`  
  eine globale Funktion zum synchronen und asynchronen nachladen von Dateien mittels AJAX. Wird die Funktion mit dem Attribut `async=true` (default) aufgerufen, so wird
  die, in der URL angegebene Datei angefragt und geladen. Bei Erfolg wird die `onload` Function als Callback aufgerufen und der Inhalt der Datei als Attribut übergeben.
  Bei einem Fehlschlag, wird die Funktion `onerror` aufgerufen. Beide Funktionen sollten beim Aufruf der loadFile Funktion übergeben werden.  
  Wird sie mit dem Attribut `async=false` aufgerufen, so werden beide Callback-Funktionen nicht benötigt und können mit `null` gefüllt werden. Die Funktion gibt dann den
  Dateiinhalt direkt zurück. Das synchrone Nachladen von Daten ist allerdings zu vermeiden, da während des Ladevorgangs das gesamte Skript getoppt wird und der Browser für
  diesen Zeitraum meist vollständig einfriert. Daher werden von einigen Browsern synchrone Anfragen direkt blockiert. Beachten Sie hierzu die Ausgabe in der Browserkonsole.  
  
  
* `function window.getCallingScriptPath( Number stackDepth=2 ): String`  
  mittels dieser Funktion kann durch Stacktracing ermittelt werden, welches Script eine Funktion aufgerufen hat. Zurückgegeben wird dessen URL.  
  
  
* `function window.getCurrentScriptPath(): String`  
  mittels dieser Funktion kann durch Stacktracing ermittelt werden, welches Script gerade ausgeführt wird. Zurückgegeben wird dessen URL.  
  
  
* `function window.getFilenameFromUrl( String url ): String`  
  schneidet den Dateinamen aus einer URL aus und gibt diesen zurück.    
  
  
* `function window.getFileTypeFromUrl( String url ): String`  
  schneidet die Dateiendung aus einer URL aus und gibt diese zurück.   
  
  
* `function console.debug( String message ): void`  
  zeigt eine Nachricht und die aktuelle Stackposition in der Browserkonsole an.  
  
  
* `function Array.prototype.removeDuplicates(): Array`  
  sucht und entfernt doppelte Einträge in einem Array und liefert eine bereinigte Version des ursprünglichen Arrays zurück.  
  
  
* `function Object.prototype.addProperty( String name, void value=null, function onchange=null, function getter=null, function setter=null ): void`  
  ermöglicht das hinzufügen von Attributen zu einem Objekt mit einem `onchange`-Listener und benutzerdefinierten getter- und setter-Methoden.
  Kann zum erschaffen virtueller Attribute genutzt werden.    
  
  
* `function Object.prototype.addConstProperty( String name, void value=null, function getter=null ): void`  
  ermöglicht das hinzufügen von unveränderlichen Attributen zu einem Objekt mit einer benutzerdefinierten getter-Methode.
  Kann zum erschaffen virtueller Attribute genutzt werden.  
  
  
* `function Object.prototype.toJson(): String`  
  wandelt ein Objekt in einen JSON-String um
  
  
* `function String.prototype.decodeJson(): Object`  
  wandelt einen JSON-String in ein Objekt um  
  
  
* `function String.prototype.encodeBase64(): String`  
  encodiert einen Datensatz zu Base64. Diese Funktion kann im Gegensatz zum nativen Base64-Encoder von JavaScript auch mit binären Daten umgehen.  
  
  
* `function String.prototype.decodeBase64(): String`  
  decodiert einen Base64-String zu binären Daten. Diese Funktion kann im Gegensatz zum nativen Base64-Decoder von JavaScript auch mit binären Daten umgehen.  


* `function String.prototype.replaceAll( String search, String replacement ): String`  
  ersetzt alle Vorkommen eines Teilstrings  


* `function String.prototype.replaceLast( String search, String replacement ): String`  
  ersetzt nur das letzte Vorkommen eines Teilstrings
