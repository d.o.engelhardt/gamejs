/**

  ▄█▀▀█▄                              ██ ▄█▀▀▀▀█▄
 ██    ▀▀  ▄▄▄▄  ▄▄▄▄▄▄▄   ▄▄▄▄       ██ ██▄▄▄ ▀▀
 ██  ▀▀██  ▄▄▄██ ██ ██ ██ ██  ██      ██  ▀▀▀███▄
 ▀█▄  ▄██ ██  ██ ██ ██ ██ ██▀▀▀▀  ██  ██ ██    ██
   ▀▀▀ ▀▀  ▀▀▀▀▀ ▀▀ ▀▀ ▀▀  ▀▀▀▀    ▀▀▀▀   ▀▀▀▀▀▀
 Version 1.0
 (c) 2021 - Oliver Engelhardt

 Licensed under the Apache License, version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

**/

"use strict";

(function(){
	/// globals --------------------------------------------------------------------------------------------------------
		window.games = [];

	/// private attributes ---------------------------------------------------------------------------------------------
		const debug = false;
		const pageRoot = window.location.href.substr( 0, window.location.href.lastIndexOf("/")+1 );
		const style = document.createElement("style");

	/// public additional function set ---------------------------------------------------------------------------------
		Object.prototype.addProperty = function( name, value=null, onchange=null, getter=null, setter=null ){
			const obj = this;
			let v = null;
			if( onchange===null && getter===null && setter===null ){ obj[name] = value; return; }
			Object.defineProperty( obj, name, {
				get: function(){ if( getter ){ return getter(v) }else{ return v; } },
				set: function( newValue ){ if( setter ){ v=setter(newValue); }else{ v=newValue; } if( onchange ){ onchange(newValue); } },
				enumerable: true, configurable: true
			} );
			if( value!==null ){ obj[name] = value; }
		};
		Object.prototype.addConstProperty = function( name, value=null, getter=null ){
			const obj = this;
			let v = value;
			Object.defineProperty( obj, name, {
				get: function(){ if( getter ){ return getter(v) }else{ return v; } },
				set: function(){ console.warn( "unable to assign value to constant: "+name ) },
				enumerable: true, configurable: true
			} );
		};
		Object.prototype.toJson = function(){ return JSON.stringify( this ); };

		Array.prototype.removeDuplicates = function(){
			const array = [...this];
			array.filter( function(item, pos){ return array.indexOf(item) === pos; } );
			return array;
		};

		String.prototype.encodeBase64 = function(){
			const b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
			let output = "", i = 0;
			while( i<this.length ){
				const byte1 = this.charCodeAt(i++) & 0xff;
				const byte2 = this.charCodeAt(i++) & 0xff;
				const byte3 = this.charCodeAt(i++) & 0xff;
				const enc1 = byte1 >> 2;
				const enc2 = ((byte1 & 3) << 4) | (byte2 >> 4);
				let enc3, enc4;
				if( isNaN(byte2) ){ enc3 = enc4 = 64; }
				else{
					enc3 = ((byte2 & 15 )<<2) | (byte3 >> 6);
					if( isNaN(byte3) ){ enc4 = 64; }
					else{ enc4 = byte3 & 63; }
				}
				output += b64.charAt(enc1) + b64.charAt(enc2) + b64.charAt(enc3) + b64.charAt(enc4);
			}
			return output;
		};
		String.prototype.decodeBase64 = function(){
			const b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
			let output = "", i = 0;
			do{
				const byte1 = b64.indexOf( this.charAt(i++) );
				const byte2 = b64.indexOf( this.charAt(i++) );
				const byte3 = b64.indexOf( this.charAt(i++) );
				const byte4 = b64.indexOf( this.charAt(i++) );
				const a = ((byte1&0x3F)<<2) | ((byte2>>4)&0x3 );
				const b = ((byte2&0xF )<<4) | ((byte3>>2)&0xF );
				const c = ((byte3&0x3 )<<6) | (byte4&0x3F);
				output += String.fromCharCode(a) + (b?String.fromCharCode(b):'') + (c?String.fromCharCode(c):'');
			}while( i<this.length );
			return output;
		};
		String.prototype.decodeJson = function(){ return JSON.parse( this ); };
		String.prototype.replaceAll = function(search, replacement){ const target = this; return target.replace(new RegExp(search, 'gm'), replacement); };
		String.prototype.replaceLast = function( what, replacement ){ const pcs = this.split(what); const lastPc = pcs.pop(); return pcs.join(what) + replacement + lastPc; };

		console.debug = function( message ){ debug?console.trace( message ):console.error( message ); };

		window.getCallingScriptPath = function( stackDepth=2 ){
			const scriptPaths = [];
			const regex = /([\w\d]+:\/\/[\w\d\/.]*\.js):/gm;
			try { throw new Error(); }
			catch(e){
				let path = null;
				while( (path = regex.exec( e.stack )) ){ scriptPaths.push( path[1] ); }
			}
			return scriptPaths[stackDepth];
		};
		window.getCurrentScriptPath = function(){ return getCallingScriptPath(1); };
		window.loadFile = function( url, onload=null, onerror=null, async=true ){
			const xhr = new XMLHttpRequest();
			let data = null;
			xhr.onreadystatechange = function() {
				if( xhr.readyState === 4 ){
					if( xhr.status === 200 ){ if( async && onload !== null ){ onload( xhr.responseText ); } }
					else{ if( async && onerror !== null ){ onerror( url ); }else{ return null; } }
				}
			};
			xhr.open( "POST", url, async );
			xhr.overrideMimeType('text/plain; charset=x-user-defined');
			let fail = true;
			try{ if(!xhr.send(null)){ fail=false; } }
			catch(e){}
			if( fail ){ if( onerror !== null ){ onerror(); } return null; }
			if( !async ){ return data; }
		};
		window.getFilenameFromUrl = function( url ){ return url.split('\\').pop().split('/').pop(); }
		window.getFileTypeFromUrl = function( url ){ return url.split('\\').pop().split('/').pop().split('.').pop(); }
		window.include = function( path ){
			console.warn( "include can only be called inside of a GAME-script" );
			//todo: maybe implement include function for global usage if needed
		};

	/// private additional function set --------------------------------------------------------------------------------
		const getAbsolutePath = function( url, forceRoot=null ) {
			if( url.indexOf("://") !== -1 ){ return url; }
			let path = "";
			if( forceRoot ){ path = forceRoot.endsWith("/") ? forceRoot.substr(0, forceRoot.lastIndexOf("/")) : forceRoot; }
			else{
				if( url.charAt(0) ==="/" ){ path = pageRoot; url=url.substr( 1, url.length ); }
				else{ path = getCallingScriptPath(); }
				path = path.substr(0, path.lastIndexOf("/"));
			}
			while( url.startsWith("../") ){ path = path.substr(0, path.lastIndexOf("/")); url = url.substr( 3, url.length ); }
			return path + "/" + url;
		}
		const loadStyle = function(){
			document.head.appendChild(style);
			style.sheet.insertRule("game { display: block; position: absolute; overflow: hidden; background: #000 }", 0);
			style.sheet.insertRule("game canvas { position: absolute; left: 0; top: 0; right: 0; bottom: 0; margin: auto; width: 100%; height: 100%; background: #000; }", 0);
			style.sheet.insertRule("fullScreenBtn { display: block; cursor: pointer; position: absolute; right: 4px; bottom: 4px; width: 32px; height: 32px; opacity: 0.75; background-color: rgba(0,0,0,0.75); background-image:  url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAQAAADZc7J/AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQflAgoVJgRBfrUzAAAAe0lEQVRIx2P4jx3kMaCB/3nYFTIxUAgG3gAWOOsiwyEk8QsYKi8wTEbi2THowwIHBiaRYu//ScMoEAfeABaGfCjrAkn61jDcodTqUTCcACO89LvAeIh4bf/tGAxgzNHyYOANYPz/H8pCLdbXoEfqfzuGECQulmJ95NaNAB58lhCUuOhrAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTAyLTEwVDIxOjM4OjA0KzAwOjAwqiTQTgAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0wMi0xMFQyMTozODowNCswMDowMNt5aPIAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC'); background-repeat: no-repeat; background-size: 20px; background-position: center; border-radius: 4px; }", 0 );
			style.sheet.insertRule("fullScreenBtn:hover { transform: scale(1.1); opacity: 1; }", 0 );
			style.sheet.insertRule("fullScreenBtn:active { background-color: #000; }", 0 );
		};

	/// input handling -------------------------------------------------------------------------------------------------
		const keyboard = new Array(256).fill(false);
		window.addEventListener( "keydown", function(event){ keyboard[event.which || event.keyCode] = true; }, false );
		window.addEventListener( "keyup", function(event){ keyboard[event.which || event.keyCode] = false; }, false );

		const mouse = { x: 0, y: 0, button: new Array(3).fill(false), wheel: 0, isTouchDevice: "ontouchstart" in document.documentElement };
		window.addEventListener( 'contextmenu', function(event){ event.preventDefault(); } );
		window.addEventListener( "mousemove", function(event){ mouse.x = event.clientX; mouse.y = event.clientY; } );
		window.addEventListener( "mousedown", function(event){ mouse.button[0] = (event.buttons&1)!==0; mouse.button[1] = (event.buttons&2)!==0; mouse.button[2] = (event.buttons&4)!==0; } );
		window.addEventListener( "mouseup", function(event){ mouse.button[0] = (event.buttons&1)!==0; mouse.button[1] = (event.buttons&2)!==0; mouse.button[2] = (event.buttons&4)!==0; } );
		window.addEventListener( "mousewheel", function(event){ const e = window.event || event; mouse.wheel += Math.max(-1, Math.min(1, e.wheelDelta || -e.detail)); });
		window.addEventListener( "DOMMouseScroll", function(event){ const e = window.event || event; mouse.wheel += Math.max(-1, Math.min(1, e.wheelDelta || -e.detail)); });
		window.addEventListener( "touchstart", function (event){ mouse.x = event.touches[0].clientX; mouse.y = event.touches[0].clientY; mouse.button[0] = event.touches.length===1; mouse.button[1] = event.touches.length===2; mouse.button[2] = event.touches.length>=3; } );
		window.addEventListener( "touchmove", function (event){ mouse.x = event.touches[0].clientX; mouse.y = event.touches[0].clientY; } );
		window.addEventListener( "touchend", function (event){ if( mouse.button[2] ){ mouse.button[0] = false; mouse.button[1] = true; mouse.button[2] = false; } else if( mouse.button[1] ){ mouse.button[0] = true; mouse.button[1] = false; mouse.button[2] = false; } else if( mouse.button[0] ){ window.setTimeout( function (){ mouse.button[0] = mouse.button[1] = mouse.button[2] = false; }, 0 ); } } );

	/// sub classes ----------------------------------------------------------------------------------------------------
		const Game = function( element, scriptUrl ){
			if( element.tagName.toUpperCase() !== "GAME" ){ console.debug( "unable to create Game: given element is not a GAME" ); return; }
			let isFullScreen = false;
			const game = this;
			game.element = element;
			game.canvas = document.createElement("canvas"); game.element.append( game.canvas );
			const fullScreenBtn = document.createElement("fullscreenBtn"); game.element.appendChild( fullScreenBtn );

			game.addProperty( "width", 640, function( newValue ){ game.element.style.width = newValue+"px"; updateCanvasSize(); } );
			game.addProperty( "height", 360, function( newValue ){ game.element.style.height = newValue+"px"; updateCanvasSize(); } );
			game.addProperty("autoSize", false, function(){ window.setTimeout( function(){updateCanvasSize();}, 0); } );
			game.addProperty("keepAspectRatio", false, function(){ updateCanvasSize(); } );
			game.addProperty( "resolutionWidth", 640, null, function(){ return game.canvas.width; }, function( newValue ){ game.canvas.width = newValue; } )
			game.addProperty( "resolutionHeight", 360, null, function(){ return game.canvas.height; }, function( newValue ){ game.canvas.height = newValue; } )
			game.addConstProperty( "fullscreen", false, function(){ return isFullScreen; } );
			game.addProperty( "showFullScreenButton", true, function( newValue ){ fullScreenBtn.style.display = newValue?"block":"none"; } )

			game.addConstProperty( "input", {} );
			game.input.addConstProperty( "key", { pressed: function( keyCode ){ return keyCode<0?false:keyCode>255?false:keyboard[keyCode]; } } );
			game.input.addConstProperty( "mouse", {} );
			game.input.mouse.addConstProperty( "x", 0, function(){ return Math.floor((mouse.x - game.canvas.getBoundingClientRect().left)/game.width*game.resolutionWidth); } );
			game.input.mouse.addConstProperty( "y", 0, function(){ return Math.floor((mouse.y - game.canvas.getBoundingClientRect().top)/game.height*game.resolutionHeight); } );
			game.input.mouse.addConstProperty( "button", { pressed: function( number ){ return number<0?false:number>2?false:mouse.button[number]; } } );
			game.input.mouse.addConstProperty( "wheel", 0, function(){ return mouse.wheel; } );
			game.input.mouse.isTouchDevice = function(){ return mouse.isTouchDevice; };

			game.resources = new Resources();

			loadScript();

			fullScreenBtn.addEventListener( "click", function(){
				isFullScreen = !isFullScreen;
				if( isFullScreen ){
					if (game.element.requestFullscreen){ game.element.requestFullscreen();	}
					else if (game.element.webkitRequestFullscreen){ game.element.webkitRequestFullscreen(); }
					else if (game.element.msRequestFullscreen){ game.element.msRequestFullscreen(); }
				}
				else{
					if (document.exitFullscreen){ document.exitFullscreen(); }
					else if (document.webkitExitFullscreen){ document.webkitExitFullscreen(); }
					else if (document.msExitFullscreen){ document.msExitFullscreen(); }
				}
				updateCanvasSize();
			} );
			function loadScript(){
				const gameId = games.length;
				const gameFunctionName = "canvasContext"+gameId;
				window.games.push( game );

				const scriptFiles = [{data: ""}];
				let waitForFiles = window.setInterval( function(){
					if( scriptFiles[0].data.trim() === "" ){ return; }
					for( let i=0; i<scriptFiles.length; i++ ){ if( scriptFiles[i].data === null ){ return; } }

					window.clearInterval( waitForFiles );
					let script = scriptFiles[0].data;
					for( let i=1; i<scriptFiles.length; i++ ){ script = script.replaceAll( scriptFiles[i].placeholder, scriptFiles[i].data ); }
					script = "function "+gameFunctionName+"(){\n"
						+ "const game = window.games["+gameId+"];\n"
						+ removeEmptyLines( script )+"}";

					const scriptTag = document.createElement("script");
					scriptTag.src = URL.createObjectURL( new Blob([script], {type: 'text/plain'}) );
					scriptTag.addEventListener("load",function(){
						window.setTimeout( gameFunctionName+"();",10 );
						if( !debug ){ URL.revokeObjectURL( scriptTag.src ); }
					});
					game.element.insertBefore( scriptTag, game.element.firstChild );
				}, 200 );
				loadScriptFile( getAbsolutePath( scriptUrl, pageRoot ), 0 );

				function removeEmptyLines( code ){ const regex = /^(?:[\t ]*(?:\r?\n|\r))+/gm; return code.replace( regex, "" ); }
				function removeComments( code ){ const regex = /\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*/gm; return code.replace( regex, "" ); }
				function findAndMarkIncludes( code, currentPath ){
					const regex = /(?:^|[;,\s]+)(include[\s]*\([\s]*"([\w\d_\-\s\.\/\:]+\.[\w\d_\-\s]*)"[\s]*\)[,;])|(?:^|[;,\s]+)(include[\s]*\([\s]*'([\w\d_\-\s.\/:]+\.[\w\d_\-\s]*)'[\s]*\)[,;])/gm;
					let output = code;
					let match = null;
					while( (match = regex.exec( code )) ){
						const placeholder = "###INCLUDE"+scriptFiles.length+"###";
						const entry = {
							placeholder: placeholder,
							url: getAbsolutePath( match[2], currentPath ),
							data: null
						};
						let entryAlreadyExists = 0;
						for( let i=0; i<scriptFiles.length; i++ ){ if( scriptFiles[i].url === entry.url ){ entryAlreadyExists = i; break; } }
						if( entryAlreadyExists ){ output = output.replace( match[1], scriptFiles[entryAlreadyExists].placeholder ); }
						else{
							output = output.replace( match[1], placeholder );
							loadScriptFile( entry.url, scriptFiles.length );
							scriptFiles.push( entry );
						}
					}
					return output;
				}
				function loadScriptFile( url, index ){
					window.loadFile( url,
						function( data ){
							data = removeComments(data);
							data = findAndMarkIncludes(data, url.substr(0, url.lastIndexOf("/") ) );
							scriptFiles[index].data = data;
						},
						function(){ scriptFiles[index].data = "ERROR"; console.debug( "failed to load script: "+url ); }
					);
				}
			}
			function updateCanvasSize(){
				if( game.autoSize ){ game.canvas.width = game.width; game.canvas.height = game.height; }
				const rect = isFullScreen?{width: screen.width, height: screen.height}:game.element.getBoundingClientRect();
				game.canvas.style.width=game.keepAspectRatio?(rect.width>rect.height?"unset":"100%"):"100%";
				game.canvas.style.height=game.keepAspectRatio?(rect.width>rect.height?"100%":"unset"):"100%";
			}
		};
		const Resources = function(){
			// privates
				const resources = this;
				const cache = {};
				const queue = {};
				function imageFromData( data, type, callback ){
					const canvas = document.createElement( "canvas" );
					const img = new Image();
					img.onload = function(){
						canvas.width = img.width;
						canvas.height = img.height;
						canvas.getContext("2d").drawImage( img, 0, 0 );
						callback( canvas );
					};
					img.src = "data:image/"+type+";base64," + data.encodeBase64();
					return 0;
				}

			// publics
				resources.addConstProperty( "loadingStatus", 0, function(){
					const cacheSize = Object.keys(cache).length;
					const queueSize = Object.keys(queue).length;
					if( cacheSize === 0 ){ return 0; }
					if( queueSize === 0 ){ return 1; }
					return cacheSize/(cacheSize+queueSize);
				} );

				resources.get = function( name ){ return cache[name]?cache[name]:null; }
				resources.loadFile = function( name, url ){
					queue[name] = url = getAbsolutePath( url, pageRoot );
					window.loadFile( url,
						function( data ){ cache[name] = data; delete queue[name]; },
						function(){ delete queue[name]; console.warn( "unable to load resource: "+url ); }
					)
				};
				resources.loadImage = function( name, url ){
					queue[name] = url = getAbsolutePath( url, pageRoot );
					window.loadFile( url,
						function( data ){ imageFromData( data, getFileTypeFromUrl(url.toLowerCase()), function( image ){ cache[name] = image; delete queue[name]; }); },
						function(){ delete queue[name]; console.warn( "unable to load resource: "+url ); }
					)
				};
		};

	/// events ---------------------------------------------------------------------------------------------------------
		window.addEventListener( "load", function(){
			loadStyle();
			const gameElementsFound = document.getElementsByTagName("game");
			for( let i=0; i<gameElementsFound.length; i++ ){ if( (gameElementsFound[i].getAttribute("script")) !== null ){ new Game( gameElementsFound[i], gameElementsFound[i].getAttribute("script") ); } }
		} );

	/// debug stuff ----------------------------------------------------------------------------------------------------
		if( debug ){
			const debugWindow = document.createElement("div");
			debugWindow.style = "position: fixed; right: 0; bottom: 0; width: 320px; height: 180px; background: #000; border: solid 1px #fff; color: #fff; font-family: monospace;";
			document.body.appendChild( debugWindow );
			window.debugInfo = function( msg ){ debugWindow.innerHTML = msg.replaceAll("\n", "<br>"); }
		}
		else{
			window.debugInfo = function( msg ){};
		}
})();
